#include<iostream>

using namespace std;

/* fungsi untuk menemukan nilai maximum dari array
dengan membandingkan nilai dalam array */
int maxInt( int n, int arr[] ) 
{
  if (n == 0) 
    return arr[0];

  int int_max = maxInt( n - 1, arr );
  
  if (int_max > arr[n]) 
    return int_max;
  else
    return arr[n];
}

//fungsi main
int main()
{
  int n;
  cin >> n;
  int arr[n];
  for (int i = 0; i < n; i++)
  {
    cin >> arr[i];
  }
  cout << "the biggest integer in the array is " << maxInt( n - 1, arr ) << endl;
}