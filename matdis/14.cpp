#include <bits/stdc++.h>
using namespace std;

int mode, frequence;

//fungsi untuk print modus yang didapatkan dari counting array
void printMode( int first, int *arr2, int int_max )
{
  if(frequence == arr2[first])
  {
    cout << first+1 << " ";
	}
  
  if(first == int_max-1)
  {
    cout << endl;
    return;
  }
  
  printMode( first + 1, arr2, int_max );
}

//fungsi untuk mendapatkan modus
void countingArray( int first, int *arr2, int int_max )
{
  if(frequence < arr2[first])
  {
		frequence = arr2[first];
		mode = first;
	} 
  
  if(first == int_max - 1)
  {
    printMode (0, arr2, int_max );
    return;
  }
  
  countingArray( first + 1, arr2, int_max );
}

/* fungsi untuk mulai menemukan modus dengan cara 
mengisi counting array sesuai frekuensi */
void findMode( int *arr, int n, int first, int int_max )
{
  int arr2[int_max + 1];
  memset( arr2, 0, sizeof(arr2) );

  for(int i = 0; i < n; i++)
  {
    arr2[arr[i] - 1]++;
  }

  frequence = arr2[0];
  countingArray( 0, arr2, int_max );
}

//fungsi main
int main(void){
  int n;
  cin >> n;
  
  int arr[n];
  
  for (int i = 0; i < n; i++)
  {
    cin >> arr[i];
  
  }    
  sort(arr, arr + n);
  int int_max = arr[n - 1];
  
  cout << "mode = ";
  findMode( arr, n, 0, int_max );
}